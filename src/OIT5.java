import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.pow;

/**
 * Вариант 6. ln^2(x) - 10cos^2(x). Метод деления отрезка пополам.
 */
public class OIT5 {
    public static void main(String[] args) {
        //исходные данные
        final int a = 2;
        final int b = 10;
        //ln^2(x) - 10cos^2(x)
        final Function<Double, Double> f = x -> pow(log(x), 2) - 10 * pow(cos(x), 2);
        //проанализировать количкство итерации в зависимости ε
        final double[] es = new double[]{10e-2, 10e-3, 10e-4, 10e-5};

        final double h = 0.1;

        //Вывод функции
        Console.printFunction(f, a, b, h);
        Chart.printFunctionGraphically(f, a, b, h);

        Scanner sc = new Scanner(System.in);
        Console.question("Все минимумы?", () -> {
            System.out.println("Левая граница интервала:");
            double localA = sc.nextDouble();
            System.out.println("Правая граница интервала:");
            double localB = sc.nextDouble();
            double[] it = new double[es.length];
            List<ResultContext> results = new ArrayList<>(es.length);
            for (double e : es) {
                results.add(findMin(f, localA, localB, e));
            }
            printResult(f, a, b, h, results);
        });


    }

    private static ResultContext findMin(Function<Double, Double> f, double a, double b, double e) {
        ResultContext resultContext = new ResultContext();
        resultContext.e = e;
        do {
            double x1 = (a + b - e) / 2;
            double x2 = (a + b + e) / 2;
            double y1 = f.apply(x1);
            double y2 = f.apply(x2);
            if (y1 > y2) {
                a = x2;
            } else {
                b = x1;
            }
            //подсчет итераций
            resultContext.itCount++;
        } while ((b - a) > 2 * e);

        return resultContext.setResult((a + b) / 2);
    }

    private static void printResult(Function<Double, Double> f, int a, int b, double h, List<ResultContext> results) {
        StringBuilder title = new StringBuilder("Количество итерация для ε: ");
        double[] IT = new double[results.size()];
        for (int i = 0; i < results.size(); i++) {
            ResultContext result = results.get(i);
            System.out.printf("Минимум в x = %.5f, при ε = %1.0e\n", result.result, result.e);
            IT[i] = result.itCount;
            title.append("ε[").append(i).append("] = ").append(String.format("%1.0e", result.e)).append(";");
        }
        double[] I = IntStream.range(0, IT.length).mapToDouble(i -> i).toArray();
        Chart.printGraph(title.toString(), "it(ε)", I, IT);
    }

    private static class ResultContext {
        double result;
        int itCount;
        double e;

        public ResultContext setResult(double result) {
            this.result = result;
            return this;
        }
    }
}
