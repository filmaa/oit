import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import java.text.DecimalFormat;

public class OIT1 {
    private static final DecimalFormat CONSTANT = new DecimalFormat("#0.000");
    private static final DecimalFormat BELOW_CONSTANT = new DecimalFormat("#0.00");
    private static final DecimalFormat TILL_TWO = new DecimalFormat("#0.0");

    public static void main(String[] args) {

        double[][] A = new double[][]{
                {-2.23, 1, 0, 0, 0},
                {1, -2, 1, 0, 0},
                {0, 1, -2, 1, 0},
                {0, 0, 1, -2, 1},
                {0, 0, 0, 1, -2.23}};
        double[] b = new double[]{0, 3, 3, 3, 0};
        double e = 0.0001;
        int n = 5;
        double[] x = new double[n];
        double[] x0 = new double[n];

        System.out.println("Метод простой интерации");
        printMatrix(A, b);
        double[] wData = new double[10];
        double[] itData = new double[10];
        int helpCount = 0;

        for (double w = 0.2; w <= 2; w += 0.2) {
            System.out.println();
            System.out.print("w = " + TILL_TWO.format(w));
            for (int i = 0; i < n; i++) {
                x[i] = 1;
                x0[i] = 1;
            }
            int it = 0;
            double de;
            double d;
            double s;
            do {
                de = 0;
                for (int i = 0; i < n; i++) {
                    s = b[i];
                    for (int j = 0; j < n; j++) {
                        if (j != i) {
                            s -= A[i][j] * x0[j];
                        }
                    }
                    s /= A[i][i];
                    s = w * s + (1 - w) * x[i];
                    d = Math.abs(x[i] - s);
                    x[i] = s;
                    if (d > de) {
                        de = d;
                    }
                }
                it += 1;
                for (int i = 0; i < n; i++) {
                    x0[i] = x[i];
                }

            } while (/*it < 100 &&*/ de >= e);

            wData[helpCount] = w;
            itData[helpCount] = it;
            helpCount += 1;
            System.out.println(", it = " + it);

        }
        // Create Chart
        XYChart chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", wData, itData);

        // Show it
        new SwingWrapper(chart).displayChart();
    }

    private static void printMatrix(double[][] a, double[] b) {
        for (int k = 0; k < 5; k++) {
            for (int m = 0; m < 5; m++) {
                if (a[k][m] < 0) {
                    System.out.print(BELOW_CONSTANT.format(a[k][m]) + " ");
                } else {
                    System.out.print(CONSTANT.format(a[k][m]) + " ");
                }
            }
            if (b[k] < 0) {
                System.out.println("   " + BELOW_CONSTANT.format(b[k]));
            } else {
                System.out.println("   " + CONSTANT.format(b[k]));
            }
        }
    }
}