import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;

public class OIT2 {
    public static void main(String[] args) {
        int m = 11;
        int n = 2;
        double[] x = new double[m];
        double[] y = new double[m];
        double a = 3;
        double b = 6;

        for (int i = 0; i < m; i++) {
            x[i] = a + (i - 1) * (b - a) / (m - 1);
            System.out.println(x[i]);
            y[i] = Math.log(x[i]) - 5 * Math.pow(Math.sin(x[i]), 2);
        }
        System.out.println("Линейная PNL");

//        Во всех вариантах (табл. 3.1.) требуется аппроксимировать заданную
//        исход-ную функцию f(x) многочленом на интервале [a, b]. Задано количество неиз-вестных параметров n,
//        вид аппроксимации и m – количество точек, в кото-рых задана функция. Таблица исходной функции
//        yi=f(xi) вычисляется в точ-ках   Используя полученную таблицу   требуется вычислить значения функций
//        и погреш-ность   в точках  , по-строить графики и проанализировать качество полученной аппроксимации.
        double[] N1;
        int i;
        double PNL[] = new double[m];
        Double xt = x[0];
        int k = 0;
        for (k = 1; k <= m; k++) {
            xt += x[1] - x[0];
            if (xt <= x[m - 1] && xt >= x[0]) {
                i = 2;
                while (xt > x[i]) {
                    i++;
                }
                if (xt <= x[i] && xt >= x[i - 1]) {
                    PNL[k] = y[i - 1] + (xt - x[i - 1]) * (y[i] - y[i - 1]) / (x[i] - x[i - 1]);
                }
            }
        }


        XYChart chart = new XYChartBuilder().width(800).height(600).xAxisTitle("x").yAxisTitle("y").build();

        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNW);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.getStyler().setYAxisLabelAlignment(Styler.TextAlignment.Right);

        chart.addSeries("PNL", x, PNL);
        chart.addSeries("real", x, y);
        new SwingWrapper<XYChart>(chart).displayChart();

        double[] xj = new double[21];
        double[] dj = new double[21];
        double[] yj = new double[21];
        double[] fj = new double[21];

        for (i = 0; i < 21; i++) {
            xj[i] = a + (i - 1) * (b - a) / 20;
            yj[i] = Math.log(xj[i]) - 5 * Math.pow(Math.sin(xj[i]), 2);
        }

        for (k = 0; k <= 20; k++) {
            xt += xj[1] - xj[0];
            if (xt <= xj[20] && xt >= xj[0]) {
                i = 2;
                while (xt > xj[i]) {
                    i++;
                }
                if (xt <= xj[i] && xt >= xj[i - 1]) {
                    fj[k] = yj[i - 1] + (xt - xj[i - 1]) * (yj[i] - yj[i - 1]) / (xj[i] - xj[i - 1]);
                }
            }
        }
        double sumD = 0;
        for (i = 0; i < 21; i++) {
            dj[i] = Math.abs(yj[i] - fj[i]);
            System.out.println(dj[i]);
            sumD += dj[i];
        }
        PNL[0] = yj[0];
        PNL[10] = yj[20];
        xj[0] = x[0];
        x[10] = xj[20];
        System.out.println("\n" + sumD);
        XYChart chart1 = new XYChartBuilder().width(800).height(600).xAxisTitle("x").yAxisTitle("y").build();

        chart1.getStyler().setLegendPosition(Styler.LegendPosition.InsideNW);
        chart1.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart1.getStyler().setYAxisLabelAlignment(Styler.TextAlignment.Right);

        chart1.addSeries("PNL", x, PNL);
        chart1.addSeries("real", xj, yj);
        new SwingWrapper<XYChart>(chart1).displayChart();


    }


}
