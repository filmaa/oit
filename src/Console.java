import java.util.Scanner;
import java.util.function.Function;

public class Console {

    public static void printFunction(Function<Double, Double> f, int a, int b, double h) {
        for (double i = a; i <= b; i += h) {
            System.out.printf(" x   =   %.5f  y   =   %.5f\n", i, f.apply(i));
        }
    }

    public static void question(String question, Question q) {
        Scanner sc = new Scanner(System.in);
        String next;
        do {
            q.print();
            System.out.println(question);
            next = sc.next();
        } while (next.equals("N"));
    }

    interface Question {
        void print();
    }


}
