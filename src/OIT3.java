import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;

import java.math.BigDecimal;

public class OIT3 {
    static int a = 3;
    static int b = 6;
    static double[] x = new double[21];
    static double[] y = new double[21];
    static double[] ydx = new double[21];
    static double[] ydxdx = new double[21];
    static double[] ydxFormula = new double[21];
    static double[] ydxdxFormula = new double[21];
    static double[] dxDifference = new double[21];
    static double[] dxdxDifference = new double[21];

    //    variant 6 ln(x)-5(sin(x))^2
//    method integrirovania Gayssa 3
//    integral f(x)dx = -3.367
    public static void main(String[] args) {
        double h = (6 - 3.) / 20;
//        f(x)' = -10*sin(x)*cos(x)+1/x
//        f(x)'' = 10*(sin(x))^2-10*(cos(x))^2-1/x^2
//        integral f(x)dx = -7*x/2+x*log(x)+5*sin(2x)/4
        for (int j = 0; j < 21; j++) {
            x[j] = a + j * (b - a) / 20.;
            y[j] = Math.log(x[j]) - 5 * Math.pow(Math.sin(x[j]), 2);
            ydx[j] = -10 * Math.sin(x[j]) * Math.cos(x[j]) + 1 / x[j];
            ydxdx[j] = 10 * Math.pow(Math.sin(x[j]), 2) - 10 * Math.pow(Math.cos(x[j]), 2) - 1 / (x[j] * x[j]);
        }

        ydxFormula[0] = -((3 * y[0] - 4 * y[1] + y[2]) / (2 * h));
        ydxFormula[20] = (y[18] - 4 * y[19] + 3 * y[20]) / (2 * h);

        ydxdxFormula[0] = ydxdxFormula[20] = 0;

//        double[] hp = new double[3];
////        hp[0] = 0.2;
////        hp[1] = 0.1;
////        hp[2] = 0.05;


        for (int j = 1; j < 20; j++) {
            ydxFormula[j] = (y[j + 1] - y[j - 1]) / (2 * h);
            dxDifference[j] = ydx[j] - ydxFormula[j];
            ydxdxFormula[j] = (y[j - 1] - y[j] * 2 + y[j + 1]) / (h * h);
            dxdxDifference[j] = ydxdx[j] - ydxdxFormula[j];
        }
        dxDifference[0] = ydx[0] - ydxFormula[0];
        dxDifference[20] = ydx[20] - ydxFormula[20];

        for (int i = 0; i < 21; i++) {
            System.out.println("x=" + x[i] +
                    ", f=" + BigDecimal.valueOf(y[i]).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue() +
                    ", f'=" + BigDecimal.valueOf((ydx[i])).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue() +
                    ", f'=" + BigDecimal.valueOf((ydxFormula[i])).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue() +
                    ", f''=" + BigDecimal.valueOf((ydxdx[i])).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue() +
                    ", f''=" + BigDecimal.valueOf((ydxdxFormula[i])).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue());
        }
        System.out.println();

        XYChart chart = new XYChartBuilder().width(800).height(600).xAxisTitle("x").yAxisTitle("y").build();

        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNW);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.getStyler().setYAxisLabelAlignment(Styler.TextAlignment.Right);

        chart.addSeries("f", x, y);
        chart.addSeries("real f'", x, ydx);
        chart.addSeries("Гаусс 3 f'", x, ydxFormula);
        chart.addSeries("real f''", x, ydxdx);
        chart.addSeries("Гаусс 3 f''", x, ydxdxFormula);
        new SwingWrapper<XYChart>(chart).displayChart();

//        count integral
        double integralGaussa10 = countIntegral(10);
        double integralGaussa20 = countIntegral(20);
        double integralGaussa30 = countIntegral(30);
        System.out.println("integral m = 10: " + integralGaussa10);
        System.out.println("integral m = 20: " + integralGaussa20);
        System.out.println("integral m = 30: " + integralGaussa30);
        System.out.println("real integral:  -3.367");
    }

    private static double countIntegral(double m) {
        double fx = 0;
        double h = (b - a) / m;
        double x = a + h / 2;
        for (int i = 0; i < m; i++) {
            fx += math(x);
            x += h;
        }
        return fx * h;
    }

    private static double math(double x) {
        return Math.log(x) - 5 * Math.pow(Math.sin(x), 2);
    }
}
