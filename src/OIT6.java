import static java.lang.Math.exp;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Вариант 6. Решения задачи коши для системы двух уравнений.
 * Метод явной экстраполяционной схемы Адамса 2-го порядка.
 */
public class OIT6 {

    public static void main(String[] args) {
        //исходные данные
        final double a = 1;
        final double b = 3;
        //количество шагов сетки
        int nx = 10;
        //максимальное число итераций
        final int nit = 100;
        //частота вывода данных, в шагах сетки
        final int np = 3;
        final double e = 1e-3;
        //уравнения
        List<BiFunction<Double, Double[], Double>> f = new ArrayList<>(2);
        f.add((x, U) -> (U[0] * U[1]) / (exp(x) * x));
        f.add(((x, U) -> 2 * x / U[0] + 2 * U[1] - exp(x) - 1));
        final Double[] u = new Double[] { 2 * a, exp(a) };

        for (int i = 0; i < 6; i++, nx *= 10) {
            double h = (b - a) / nx;
            ResultContext context = m6(f, a, b, nx, u);
            out(context);
            outGrNorm(h, context, 10);
        }
    }

    private static ResultContext m6(List<BiFunction<Double, Double[], Double>> system, double a, double b, int nx,
            Double[] y0) {
        ResultContext context = new ResultContext();
        double h = (b - a) / nx;

        context.add(a, y0);
        Double[] yh = new Double[2];
        for (int i = 0; i < system.size(); i++) {
            BiFunction<Double, Double[], Double> f = system.get(i);
            yh[i] = y0[i] + h / 2 * f.apply(a, y0);
        }
        Double[] y1 = new Double[2];
        for (int i = 0; i < system.size(); i++) {
            BiFunction<Double, Double[], Double> f = system.get(i);
            y1[i] = y0[i] + h * f.apply(a + h / 2, yh);
        }
        double x1 = a + h;
        context.add(x1, y1);
        nx--;
        Double[] y2 = new Double[2];
        do {
            for (int i = 0; i < system.size(); i++) {
                BiFunction<Double, Double[], Double> f = system.get(i);
                y2[i] = y1[i] + h * (1.5 * f.apply(x1, y1) - 0.5 * f.apply(x1 - h, y0));
            }
            double x2 = x1 + h;
            context.add(x2, y2);
            x1 = x2;
            y0 = y1;
            y1 = y2;
        } while (--nx > 0);
        return context;
    }

    private static void out(ResultContext rs) {
        for (int i = 0; i < rs.size; i++) {
            System.out.printf("x = %f y1 = %f u1 = %f d1 = %f y2 = %f u2 = %f d2 = %f\n",
                    rs.X.get(i), rs.Y1.get(i), rs.U1.get(i), rs.D1.get(i), rs.Y2.get(i), rs.U2.get(i), rs.D2.get(i));
        }

    }

    private static void outGrNorm(double h, ResultContext context, int k) {
        Chart ch = new Chart(String.format("h = %f", h));
        double[] X = ArrayUtil.toArray(context.X, k);
        ch.addSeries("f_1(x)", X, ArrayUtil.toArray(context.U1, k));
        ch.addSeries("m7_1(x)", X, ArrayUtil.toArray(context.Y1, k));
        ch.addSeries("f_2(x)", X, ArrayUtil.toArray(context.U2, k));
        ch.addSeries("m7_2(x)", X, ArrayUtil.toArray(context.Y2, k));
        ch.displayChart();
    }

    private static class ResultContext {
        List<Double> X = new ArrayList<>();
        List<Double> Y1 = new ArrayList<>();
        List<Double> U1 = new ArrayList<>();
        List<Double> D1 = new ArrayList<>();
        List<Double> Y2 = new ArrayList<>();
        List<Double> D2 = new ArrayList<>();
        List<Double> U2 = new ArrayList<>();
        int size;

        void add(double x, Double[] y) {
            X.add(x);
            Y1.add(y[0]);
            U1.add(2 * x);
            D1.add(2 * x - y[0]);
            Y2.add(y[1]);
            U2.add(exp(x));
            D2.add(exp(x) - y[1]);
            size++;
        }

    }
}