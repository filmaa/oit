import java.util.List;
import java.util.function.Function;

/**
 * @author Branavets_AY
 */
public class ArrayUtil {

    public static double[] fromRange(double a, double b, double h) {
        int n = (int) ((b - a) / h);
        double[] arr = new double[n];
        double x = a;
        for (int i = 0; i < n; i++, x += h) {
            arr[i] = x;
        }
        return arr;
    }

    public static double[] applyFunction(Function<Double, Double> f, double[] x) {
        double[] Y = new double[x.length];
        for (int i = 0; i < Y.length; i++) {
            Y[i] = f.apply(x[i]);
        }
        return Y;
    }

    public static double[] toArray(List<Double> ds) {
        return ds.stream().mapToDouble(d -> d).toArray();
    }

    public static double[] toArray(List<Double> ds, int k) {
        int h = ds.size() / k;
        double[] arr = new double[k + 1];
        for (int i = 0, j = 0; i < ds.size(); i += h, j++) {
            arr[j] = ds.get(i);
        }
        return arr;

    }
}
