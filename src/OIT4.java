import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.lang.Math.abs;
import static java.lang.Math.log;
import static java.lang.Math.pow;
import static java.lang.Math.sin;

/**
 * Варинт 6. ln(x)-5sin^2(x). Интеравал a:2, b:6. Метод деления отрезка пополам. ε = 10^-4
 */
public class OIT4 {

    public static void main(String[] args) {
        //исходные данные
        final int a = 2;
        final int b = 6;
        //ln(x)-5sin^2(x)
        final Function<Double, Double> f = x -> log(x) - 5 * pow(sin(x), 2);
        //ε = 10^-4
        final double e = 10e-4;
        //шаг
        final double h = 0.1;

        //Вывод функции
        Console.printFunction(f, a, b, h);
        Chart.printFunctionGraphically(f, a, b, h);

        Scanner sc = new Scanner(System.in);
        Console.question("Все корни?", () -> {
            System.out.println("Левая граница интервала:");
            double localA = sc.nextDouble();
            System.out.println("Правая граница интервала:");
            double localB = sc.nextDouble();
            ResultContext resultContext = findRoot(f, localA, localB, e);
            printResult(f, a, b, h, resultContext);
        });
    }

    private static void printResult(Function<Double, Double> f, int a, int b, double h, ResultContext result) {
        System.out.printf("Корень уравнения: %.5f\n", result.getResult());

        Chart chart = new Chart(String.format("Метод деления отрезка пополам на отрезке [%.2f, %.2f]",result.a,result.b));
        //график функции
        double[] X0 = ArrayUtil.fromRange(a, b, h);
        double[] Y0 = ArrayUtil.applyFunction(f, X0);
        chart.addSeries("f(x) - график функции", X0, Y0);

        double[] X = result.X.stream().mapToDouble(Double::doubleValue).toArray();
        double[] Y = result.Y.stream().mapToDouble(Double::doubleValue).toArray();
        chart.addSeries("график сходимости", X, Y);

        double[] D = result.D.stream().mapToDouble(Double::doubleValue).toArray();
        double[] I = IntStream.range(0, D.length).mapToDouble(i -> i).toArray();
        //chart.addSeries("d(I) - растояние от номера итерации", I, D);
        Chart.printGraph("Зависимость разности корней от номера итерации", "d(I)", I, D);

        chart.displayChart();
    }


    /**
     * Нахождение корня методом деления отрезка пополам
     *
     * @param f  - Функция
     * @param a  - левая граница интервала
     * @param b  - правая граница интеравала
     * @param e  - точночть
     * @return корень вблизи между интервалами
     */
    private static ResultContext findRoot(Function<Double, Double> f, double a, double b, double e) {
        ResultContext resultContext = new ResultContext();

        resultContext.a = a;
        resultContext.b = b;

        double x0 = a;
        double x1 = b;
        double result;


        double d;
        do {
            double x2 = (x0 + x1) / 2;

            double y0 = f.apply(x0);
            double y2 = f.apply(x2);

            if (y0 * y2 > 0) {
                x0 = x2;
            } else {
                x1 = x2;
            }

            result = (x0 + x1) / 2;
            if (!resultContext.X.isEmpty()) {
                resultContext.D.add(abs(result - resultContext.X.get(resultContext.X.size() - 1))); //расстояние между итерациями
            }
            resultContext.X.add(result); //запоминаем текущий корень
            resultContext.Y.add(f.apply(result)); //значение функции в этой точке


            d = x1 - x0;
        } while (abs(d) > e);

        return resultContext.setResult(result);
    }

    static class ResultContext {
        double result;
        double a;
        double b;
        List<Double> X = new ArrayList<>();
        List<Double> Y = new ArrayList<>();
        List<Double> D = new ArrayList<>();

        public double getResult() {
            return result;
        }

        public ResultContext setResult(double result) {
            this.result = result;
            return this;
        }
    }

}
