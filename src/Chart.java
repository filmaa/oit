import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.XYStyler;

import java.util.function.Function;

/**
 * @author Branavets_AY
 */
public class Chart {

    private final XYChart chart;

    public Chart(String title) {
        chart = new XYChartBuilder().title(title).width(800).height(600).xAxisTitle("x").yAxisTitle("y").build();

        XYStyler styler = chart.getStyler();
        styler.setLegendPosition(Styler.LegendPosition.InsideNW);
        styler.setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        styler.setYAxisLabelAlignment(Styler.TextAlignment.Right);
    }

    public static void printFunctionGraphically(Function<Double, Double> f, double a, double b, double h) {
        double[] X = ArrayUtil.fromRange(a, b, h);
        double[] Y = ArrayUtil.applyFunction(f, X);
        Chart chart = new Chart("f(x)");
        chart.addSeries("f(x)", X, Y);
        chart.displayChart();
    }

    public static void printGraph(String title, String seriesName, double[] xData, double[] yData) {
        Chart chart = new Chart(title);
        chart.addSeries(seriesName, xData, yData);
        chart.displayChart();
    }

    public XYSeries addSeries(String seriesName, double[] xData, double[] yData) {
        return chart.addSeries(seriesName, xData, yData);
    }

    public void displayChart() {
        new SwingWrapper<>(chart).displayChart();
    }
}
